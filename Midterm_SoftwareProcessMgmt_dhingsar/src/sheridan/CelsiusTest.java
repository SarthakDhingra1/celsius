package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author sarthak dhingra
 * student id: 991521229
 *
 */
public class CelsiusTest {

	@Test
	public void testFromFarenheitRegular() {
		int result=Celsius.fromFarenheit(32);//PasswordValidator.isVaildLength("1234567890");
		assertTrue("The celsius value is invalid",result==0);
	}
	
	@Test
	public void testFromFarenheitException() {
		int result = Celsius.fromFarenheit(-1);
		assertFalse("The celsius value is invalid",result==-19);
	}
	
	@Test
	public void testFromFarenheitBoundaryIn() {
		int result = Celsius.fromFarenheit(70);
		assertTrue("The celsius value is invalid",result==21);

	}
	
	@Test
	public void testFromFarenheitBoundaryOut() {
		int result = Celsius.fromFarenheit(71);
		assertTrue("The celsius value is invalid",result==21);

	}

}
