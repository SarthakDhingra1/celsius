package sheridan;
/**
 * 
 * @author sarthak dhingra
 * student id: 991521229
 *
 */
public class Celsius {
	
	public static int fromFarenheit(int temp) {
		
		return Math.round(((temp-32)*5)/9);
	}

}
